
function sumBasketballPoints(twoPoints, threePoints) {
        const totTwoPoints = 2*twoPoints;
        const totThreePoints = 3*threePoints;

        return totTwoPoints+totThreePoints;
}

console.log("Points(1,1) ➞ " + sumBasketballPoints(1,1));
console.log("Points(7,5) ➞ " + sumBasketballPoints(7,5));
console.log("Points(38,8) ➞ " + sumBasketballPoints(38,8));
console.log("Points(0,1) ➞ " + sumBasketballPoints(0,1));
console.log("Points(0,0) ➞ " + sumBasketballPoints(0,0));