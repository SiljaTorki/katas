# Katas

## Basketball Points - Katas 1 (13.01.2022)

## Description
Counting points for a basketball game, given the amount of 2-pointers scored and 3-pointers scored, find the final points for the team and return that value.

Examples:

points(1, 1) ➞ 5

 
points(7, 5) ➞ 29


points(38, 8) ➞ 100

 
points(0, 1) ➞ 3

 
points(0, 0) ➞ 0


## Visuals
Home page:

![BasketballIndex](./images/BascketballIndex.PNG)

Console:

![BasketballConsole](./images/BasketballConsole.PNG)

## Project status
The function that calculate the basketball pionts is comlete. To make the projects fully complete HTML and CSS neds to be added. 

