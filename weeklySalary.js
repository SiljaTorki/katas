let showConsoleE = document.getElementById("showConsole");
let resultE = document.getElementById("result");

let workArray = [8, 8, 8, 8, 8, 0, 0];
let workArray1 = [10, 10, 10, 0, 8, 0, 0];
let workArray2 = [0, 0, 0, 0, 0, 12, 0];
let salary = 0;

const weeklySalary = () => {
    for (let index = 0; index < workArray1.length; index++) {
        let hour = workArray1[index];

        if (hour < 0) {
            alert(`${hour} does not count, has to be greater than or equal to 0`)
        }

        if(index <= 4  && hour != 0) {
            salary += 10*8;
            if (hour > 8) {
                salary += 15*(hour-8);
            }
        } 
    
        if (index > 4 && hour != 0) {
            salary += 20*8;
            if (hour > 8) {
                salary += 30*(hour-8);
            }
        } 

    }
    resultE.innerText += " ["+ workArray1 + "] ➞ " + salary;
}
//console.log(weeklySalary());

showConsoleE.addEventListener("click", weeklySalary);