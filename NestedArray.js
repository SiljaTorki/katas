const array = [1, [2,3]];
const array2 = [1, [2, [3, 4]]];
const array3 = [1, [2, [3, [4, [5, 6]]]]];
const array4 = [1, [2], 1, [2], 1];
const empty = [];


const getLength = (array) => {
    let newArray = array.flat(Infinity);
    console.log(newArray.length);
}

getLength(array);
getLength(array2);
getLength(array3);
getLength(array4);
getLength(empty);


//Recrusive
//const getLength = (arrayElement) => {
    //let count = 0;
    //for (elem of arrayElement) {
        //if (elem instanceof Array) {
            //count += getLength(elem);
        //}
        //else {
           // count++;
        //}
    //}
    //return count;
//}
